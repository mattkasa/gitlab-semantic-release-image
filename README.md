gitlab-semantic-release-image
=============================

A small Docker image based on `node:10-alpine` containing `semantic-release` and
the `@semantic-release/gitlab` plugin.

Usage
-----

```
docker run registry.gitlab.com/mattkasa/gitlab-semantic-release-image:latest
```
