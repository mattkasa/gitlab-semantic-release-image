FROM node:10-alpine
RUN apk add --no-cache \
      ca-certificates \
      git
ADD . /gitlab-semantic-release
WORKDIR /gitlab-semantic-release
RUN npm install --production
RUN ln -s /gitlab-semantic-release/node_modules/.bin/semantic-release /usr/bin/semantic-release
COPY --from=registry.gitlab.com/gitlab-org/release-cli:v0.4.0 /usr/local/bin/release-cli /usr/bin/release-cli
CMD ["semantic-release"]
